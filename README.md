## jls - simple file list

jls(1) is a simple utility I created because I cannot
remember ls(1) options between various UN\*X Systems.
I also wanted a default of printing only Last Modification
Date/Time and File Name.

It also avoids command line size by reading the files to
process from stdin or a Text File.  Some UN\*X Systems have
a rather short Command Line Buffer.

Repositories:
* [gitlab repository](https://gitlab.com/jmcunx1/jls) (this site)
* gemini://gem.sdf.org/jmccue/computer/repoinfo/jls.gmi (mirror)
* gemini://sdf.org/jmccue/computer/repoinfo/jls.gmi (mirror)

[j\_lib2](https://gitlab.com/jmcunx1/j_lib2)
is an **optional** dependency.

[Automake](https://en.wikipedia.org/wiki/Automake)
only confuses me, but this seems to be good enough for me.

To compile:
* If "DESTDIR" is not set, will install under /usr/local
* Execute ./build.sh to create a Makefile,
execute './build.sh --help' for options
* Then make and make install
* Works on Linux, BSD and AIX

To uninstall:
* make (see compile above)
* As root: 'make uninstall' from the source directory

This is licensed using the
[ISC Licence](https://en.wikipedia.org/wiki/ISC_license).
